import threading
from app import app
from app import bot

if __name__ == "__main__":
    # Run telegram bot in other thread
    bot_polling_thread = threading.Thread(target=bot.polling)
    bot_polling_thread.setDaemon(True)
    bot_polling_thread.start()

    # Run Flask application
    app.run()
