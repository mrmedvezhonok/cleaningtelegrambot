from app import app
from app import bot
from app import Order

from flask import request
from flask import jsonify
from flask import render_template

from errors import OrderMissingDataError

from utils import time_str2time
from utils import error_response

from telebot.types import InlineKeyboardMarkup
from telebot.types import InlineKeyboardButton


@app.route('/', methods=['GET'])
def main():
    return render_template('form.html')


@app.route('/bot-api/order/', methods=['POST'])
def post_order():
    """
        {
            "address": "2-я Песчаная ул., 4А, Москва, Россия, 125252",
            "cost": 550,
            "time": "18:15",
            "customer_name": "Василий Беляев",
            "todo_list": "Почистить диван, очистить кафель",
            "comment": "Это важный комментарий, просьба его учесть"
        }
        """

    # Get data from request.
    data = request.get_json()
    address = data.get('address')
    cost = data.get('cost')
    time = data.get('time')
    customer_name = data.get('customer_name')
    todo_list = data.get('todo_list')
    comment = data.get('comment')

    try:
        new_order = Order.create_order(time_str2time(time), address, customer_name, todo_list, cost, comment)
    except OrderMissingDataError as e:
        print(e)
        return error_response(400, 'Missing parameter.')
    except TypeError as e:
        print(e)
        return error_response(400, "Incorrect 'time' parameter.")

    # get response message text from Order object.
    response_message = str(new_order)

    # Create inline keyboard
    keyboard = InlineKeyboardMarkup()
    buttons = list()

    # Buttons contains commands with order id to get this data at click event.
    buttons.append(InlineKeyboardButton(text="Выполнить", callback_data=f'/take {new_order.id}'))
    buttons.append(InlineKeyboardButton(text="Подробнее", callback_data=f'/more {new_order.id}'))
    keyboard.add(*buttons)

    # Send message with order to the telegram group.
    bot.send_message(app.config['GROUP_CHAT_ID'], response_message, reply_markup=keyboard)

    # return response_message
    return jsonify({
        "ok": 'true',
        "order_id": new_order.id
    })
