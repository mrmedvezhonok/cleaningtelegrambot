from datetime import datetime as dt

from app import db

from errors import OrderMissingDataError

from data import smiley


class Employee(db.Model):
    __tablename__ = 'employee'

    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, unique=True, nullable=False)
    username = db.Column(db.String(300), unique=True, nullable=True)
    last_name = db.Column(db.String(300), nullable=True)
    first_name = db.Column(db.String(300), nullable=True)

    registration_dt = db.Column(db.DateTime, nullable=False, default=dt.utcnow)
    orders_list = db.relationship("Order", backref="employee")

    def __init__(self, chat_id: int, first_name: str, last_name: str, username: str):
        self.chat_id = chat_id
        self.first_name = first_name
        self.last_name = last_name
        self.username = username

    def __repr__(self):
        return f"<Employee {self.id}, {self.chat_id}, {self.username}, {self.first_name}, {self.last_name}, {self.registration_dt}>"

    @staticmethod
    def is_registered(chat_id):
        query = Employee.query.filter_by(chat_id=chat_id).limit(1).first()
        return query is not None

    @staticmethod
    def get(chat_id):
        query = Employee.query.filter_by(chat_id=chat_id).limit(1).first()
        return query

    @staticmethod
    def register(chat_id: int, first_name: str, last_name: str, username: str):
        new_employee = Employee(chat_id, first_name, last_name, username)
        db.session.add(new_employee)
        db.session.commit()
        return new_employee

    def update(self, first_name: str, last_name: str, username: str):
            self.first_name = first_name
            self.last_name = last_name
            self.username = username
            db.session.commit()


class OrderStatus:
    status_names = ["Не определён", "В работе", "Оплачен "]
    min, max = 0, 2
    NOT_DEFINED = 0
    IN_WORK = 1
    PAID = 2

    @staticmethod
    def get_name(status: int):
        if status < OrderStatus.min or status > OrderStatus.max:
            raise IndexError(f"Status must be in range {min} - {max}")
        return OrderStatus.status_names[status]


class Order(db.Model):
    __tablename__ = 'order'

    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.Time, nullable=False)
    dt = db.Column(db.DateTime, nullable=False, default=dt.utcnow)
    address = db.Column(db.String(300), nullable=False)
    customer_name = db.Column(db.String(300), nullable=False)

    todo_list = db.Column(db.String(300), nullable=False)
    cost = db.Column(db.Integer, nullable=False)
    comment = db.Column(db.String(300), nullable=True)

    status_value = db.Column(db.Integer, default=0)

    employee_id = db.Column(db.Integer, db.ForeignKey('employee.id'))

    def __init__(self, order_time: time, address: str, customer_name: str, todo: str, cost: int, comment: str = None):
        self.time = order_time
        self.address = address
        self.customer_name = customer_name
        self.todo_list = todo
        self.cost = cost
        self.comment = comment

        # Validation of order parameters
        if self.time is None or self.address is None or self.customer_name is None \
                or self.todo_list is None or self.cost is None:
            raise OrderMissingDataError()

    def __repr__(self):
        return f"<Order {self.id}, {self.time}, {self.address}, {self.dt}, {self.customer_name}, {self.cost}>"

    def __str__(self):
        if self.comment is None or self.comment.strip() == '':
            comment_text = "Без комментариев."
        else:
            comment_text = str(self.comment)

        return \
            f"{smiley['clock1']} Время: {str(self.time)[:5]}.\n" \
                f"{smiley['house']} Адрес: {self.address}.\n" \
                f"{smiley['credit_card']} Стоимость: {self.cost}{smiley['ruble']}.\n" \
                f"{smiley['necktie']} Имя заказчика: {self.customer_name}.\n" \
                f"{smiley['clipboard']} Перечень: {self.todo_list}.\n" \
                f"{smiley['memo']} Комментарий: {comment_text}.\n" \
                f"{smiley['information_source']} Статус: {OrderStatus.get_name(self.status_value)}."

    @staticmethod
    def create_order(order_time: time, address: str, customer_name: str, todo: str, cost: int, comment: str = None):
        new_order = Order(order_time, address, customer_name, todo, cost, comment)
        db.session.add(new_order)
        db.session.commit()
        return new_order
