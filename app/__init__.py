from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from config import Config

import telebot

bot = telebot.TeleBot(Config.BOT_TOKEN)

app = Flask(__name__)
app.config.from_object(Config)
CORS(app)
db = SQLAlchemy(app)

from models import Order
from models import Employee

db.create_all()

# Import flask resources
from flask_resources import *

from bot_handlers import *