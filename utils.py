from flask import jsonify
from datetime import timezone
from datetime import timedelta
from datetime import datetime as dt
from datetime import time


def error_response(status_code, message=None):
    from werkzeug.http import HTTP_STATUS_CODES
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response


def utc_convert(tz_offset):
    """
    Default using utc time zone. The function convert utc tz to tz_offset.
    :param tz_offset:
    :return:
    """
    if not isinstance(tz_offset, int):
        raise TypeError("tz_offset must be int")
    td = timedelta(hours=tz_offset)
    utcnow = dt.utcnow()
    return utcnow.replace(tzinfo=timezone.utc).astimezone(timezone(td))


def time_str2time(time_str: str):
    """
    Convert string contains hours and minutes time to datetime.time object
    :param time_str: str in format "hour:minute". Example  "18:45"
    :return:
    """
    if not isinstance(time_str, str):
        raise TypeError("time_str must be str.3")
    hours, minutes = tuple(map(int, time_str.split(':')))
    return time(hour=hours, minute=minutes)
