import os

basedir = os.path.abspath(os.path.dirname(__file__))
db_dir = os.path.join(basedir, "db")
db_name = os.environ.get("DB_NAME", "cleaning_db")
if not os.path.isdir(db_dir):
    os.mkdir(db_dir)


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', 'd9875f59d50b464629bccebb46870572')

    SQLALCHEMY_DATABASE_URI = f"sqlite:///{db_dir}/{db_name}.sqlite3"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    BOT_TOKEN = os.getenv('BOT_TOKEN')
    GROUP_CHAT_ID = os.getenv('GROUP_CHAT_ID', -360876606)

# TODO set environs:
#   DB_NAME
#   BOT_TOKEN
#   GROUP_CHAT_ID
