from telebot.types import Message

from app import bot
from app import Employee


@bot.message_handler(func=lambda m: m.chat.type == 'private', commands=['register'])
def bot_command_register(msg: Message):
    # Register new employee in private chat.

    from_user = msg.from_user
    chat_id = from_user.id
    first_name = from_user.first_name
    last_name = from_user.last_name
    username = from_user.username

    # Check if the employee is registered.
    employee = Employee.get(chat_id)

    # if Employee.is_registered(chat_id):
    if employee is not None:
        bot.send_message(chat_id, "Вы уже зарегистрированы.")
        if employee.first_name != first_name or employee.last_name != last_name or employee.username != username:
            Employee.update(employee, first_name, last_name, username)
            bot.send_message(chat_id, 'Данные успешно обновлены.')

        return

    Employee.register(chat_id, first_name, last_name, username)
    bot.send_message(chat_id, "Вы успешно зарегистрированы.")


# noinspection SpellCheckingInspection
@bot.message_handler()
def bot_main_message_handler(msg: Message):
    print(msg)
    bot.send_message(msg.chat.id, "Привет")

    # PRIVATE chat
    """
    {'content_type': 'text', 'message_id': 835, 
    'from_user': {'id': 475696769, 'is_bot': False, 'first_name': 'Sergey','username': 'MeMedvezhonok', 'last_name': None, 'language_code': 'en'}, 
    'date': 1569093571, 
    'chat': {'type': 'private', 'last_name': None, 'first_name': 'Sergey', 'username': 'MeMedvezhonok', 'id': 475696769}, 
    'text': 'приуэт', 
     'json': {'message_id': 835, 
     'from': {'id': 475696769, 'is_bot': False, 'first_name': 'Sergey', 'username': 'MeMedvezhonok', 'language_code': 'en'}, 
     'chat': {'id': 475696769, 'first_name': 'Sergey', 'username': 'MeMedvezhonok', 'type': 'private'}, 
     'date': 1569093571, 'text': 'приуэт'}}
    """

    # GROUP chat
    """
    {'content_type': 'text', 'message_id': 841, 
    'from_user': {'id': 475696769, 'is_bot': False, 'first_name': 'Sergey', 'username': 'MeMedvezhonok', 'last_name': None, 'language_code': 'en'}, 
    'date': 1569094081, 
    'chat': {'type': 'group', 'last_name': None, 'first_name': None, 'username': None, 'id': -360876606, 
    'title': 'BotTestPolygon', 'all_members_are_administrators': True,     
    'text': 'привет из игруппы',     
    'json': {'message_id': 841, 
    'from': {'id': 475696769, 'is_bot': False, 'first_name': 'Sergey', 'username': 'MeMedvezhonok', 'language_code': 'en'}, 
    'chat': {'id': -360876606, 'title': 'BotTestPolygon', 'type': 'group', 'all_members_are_administrators': True}, 
    'date': 1569094081, 'text': 'привет из игруппы'}}
    """
    pass
